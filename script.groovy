def buildjar() {
    echo 'Building jar file for the application'
    sh 'mvn package'

}

def buldingimage() {
    echo 'Building docker image of the application'
    withCredentials([usernamePassword(credentialsId: 'dockerlogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
    sh 'docker build -t sureshpanasa/java-maven-app:2.0 .'
    sh "echo $PASS | docker login -u $USER --password-stdin"
    sh 'docker push sureshpanasa/java-maven-app:2.0'
    echo 'docker image is ready for use...'
    }

}

return this